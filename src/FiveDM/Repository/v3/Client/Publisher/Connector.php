<?php
/**
 * Copyright (c) 2013 Michal Havlicek <michal.havlicek@email.cz>
 */

namespace FiveDM\Repository\v3\Client\Publisher;

/**
 * @author Michal Havlicek <michal.havlicek@email.cz>
 * @package Repository
 */
class Connector
{
    /** @var array */
    protected $options = array(
        'url' => array(
            'insert' => 'https://api.topsrovnani.cz/v3/publisher/campaigns/<cid>/forms/',
            'update' => 'https://api.topsrovnani.cz/v3/publisher/campaigns/<cid>/forms/<id>',
            'verify' => 'https://api.topsrovnani.cz/v3/publisher/campaigns/<cid>/forms/<id>/verify',
            'send'   => 'https://api.topsrovnani.cz/v3/publisher/campaigns/<cid>/forms/<id>/send',
            'queue'  => 'https://api.topsrovnani.cz/v3/publisher/campaigns/<cid>/forms/<id>/queue'
    ));

    // Result statuses
    const STATUS_OK = 'success';
    const STATUS_ERROR = 'error';

    // Result codes
    const CODE_REQUEST_OK = 200;
    const CODE_REQUEST_INSERT_OK = 201;
    const CODE_REQUEST_DUPLICITY = 202;
    const CODE_REQUEST_ERROR = 500;
    const CODE_ENTITY_NOT_FOUND = 404;

    /** @var integer|null ID of advertising campaign */
    private $campaignId = NULL;

    /** @var array Defines user and password */
    private $credentials = array(
        'user'     => '',
        'password' => ''
    );

    /**
     * @param integer $campaignId ID of advertising campaign
     * @param array $credentials User and password
     */
    function __construct($campaignId, array $credentials)
    {
        $this->campaignId  = $campaignId;
        $this->credentials = $credentials;
    }

    /**
     * Connection configuration
     *
     * @param string $option Option to change
     * @param string $value New value
     */
    public function setOption($option, $value)
    {
        $this->options[$option] = $value;
    }

    /**
     * Method searches for placeholding patterns and replaces them with real values.
     *
     * @param array $parameters
     * @param string $url URL stub
     * @return string Returns URL with replaced patterns.
     */
    protected function prepareUrl(array $parameters, $url)
    {
        $url = $this->options['url'][$url];

        foreach($parameters as $key => $value) {
            $url = str_replace('<' . $key . '>', $value, $url);
        }

        return $url;
    }

    /**
     * Method creates CURL request.
     *
     * @param string $url URL of API method
     * @return \Kdyby\Curl\Request Returns CURL wrapper
     */
    protected function createConnection($url)
    {
        $request = new \Kdyby\Curl\Request($url);
        $request->options['USERPWD']  = $this->credentials['user'] . ':' . $this->credentials['password'];
        $request->options['HTTPAUTH'] = -1;
        $request->timeout = 120;

        return $request;
    }

    /**
     * Method creates new form in the repository.
     *
     * @param Model\Form $form Form that should be stored in repository
     * @return bool Returns true on success, otherwise false
     */
    public function create(\FiveDM\Repository\v3\Client\Publisher\Model\Form $form)
    {
        try {
            $response = json_decode(
                $this->createConnection($this->prepareUrl(array(
                    'cid' => $this->campaignId
                ), 'insert'))->post(http_build_query($form->toArray()))->getResponse()
            );
            return $response->status == self::STATUS_OK ? $response->data->id : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }

    /**
     * Method updates values of not-delivered form.
     *
     * @param integer $formId ID of form
     * @param Model\Form $form Form that should be updated
     * @return bool Returns true on success, otherwise false
     */
    public function update($formId, \FiveDM\Repository\v3\Client\Publisher\Model\Form $form)
    {
        try {
            $response = json_decode(
                $this->createConnection($this->prepareUrl(array(
                    'cid' => $this->campaignId,
                    'id'  => $formId
                ), 'update'))->put(http_build_query($form->toArray()))->getResponse()
            );
            return $response->status == self::STATUS_OK ? $response->data->id : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }

    /**
     * Method sends SMS verification code to customer's phone.
     *
     * @param integer $formId ID of form
     * @return bool Returns true on success, otherwise false
     */
    public function sendVerificationRequest($formId)
    {
        try {
            $response = json_decode($this->createConnection($this->prepareUrl(array(
                'cid' => $this->campaignId,
                'id'  => $formId
            ), 'verify'))->post(
                http_build_query(array('')) # TODO: Fix Entity too large hack
            )->getResponse());

            return $response->status == self::STATUS_OK ? TRUE : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }

    /**
     * Method verifies SMS code inserted by user.
     *
     * @param integer $formId ID of form
     * @param integer $smsCode SMS code
     * @return bool Returns true on success, otherwise false
     */
    public function verify($formId, $smsCode)
    {
        try {
            $response = json_decode($this->createConnection($this->prepareUrl(array(
                'cid' => $this->campaignId,
                'id'  => $formId
            ), 'verify'))->put(http_build_query(array(
                'smsCode' => $smsCode
            )))->getResponse());

            return $response->status == self::STATUS_OK ? TRUE : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }

    /**
     * Method delivers form to advertiser. This method triggers immediate push request, so no output is sent till
     * the advertiser's API confirms delivery.
     *
     * @param integer $formId ID of form
     * @return bool Returns true on success, otherwise false
     */
    public function send($formId)
    {
        try {
            $response = json_decode($this->createConnection($this->prepareUrl(array(
                'cid' => $this->campaignId,
                'id'  => $formId
            ), 'send'))->put()->getResponse());

            return $response->status == self::STATUS_OK || $response->status == 'error' && $response->code == self::CODE_REQUEST_DUPLICITY ? TRUE : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }

    /**
     * Method inserts deliver request at the end of the queue. That means, form will be delivered later.
     * Output message is sent back immediately.
     *
     * @param integer $formId ID of form
     * @return bool Returns true on success, otherwise false
     */
    public function queue($formId)
    {
        try {
            $response = json_decode($this->createConnection($this->prepareUrl(array(
                'cid' => $this->campaignId,
                'id'  => $formId
            ), 'queue'))->post(
                http_build_query(array('')) # TODO: Fix Entity too large hack
            )->getResponse());

            return $response->status == self::STATUS_OK ? TRUE : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }
}
