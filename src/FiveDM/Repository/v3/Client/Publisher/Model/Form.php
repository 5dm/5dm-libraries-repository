<?php
/**
 * Copyright (c) 2013 Michal Havlicek <michal.havlicek@email.cz>
 */

namespace FiveDM\Repository\v3\Client\Publisher\Model;

/**
 * @author Michal Havlicek <michal.havlicek@email.cz>
 * @package Repository
 */
class Form
{
    protected $customer;

    protected $values;

    protected $source;

    protected $utm;

    public static function from(array $form)
    {
        $instance = new static();

        foreach($form as $property => $value) {
            call_user_func(array($instance, 'set' . ucfirst($property)), $value);
        }

        return $instance;
    }

    public function toArray()
    {
        return array(
            'customer' => $this->customer,
            'values'   => $this->values,
            'source'   => $this->source,
            'utm'      => $this->utm
        );
    }

    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    public function setSource($source)
    {
        $this->source = $source;
    }

    public function getSource()
    {
        return $this->source;
    }

    public function setUtm($utm)
    {
        $this->utm = $utm;
    }

    public function getUtm()
    {
        return $this->utm;
    }

    public function setValues($values)
    {
        $this->values = $values;
    }

    public function getValues()
    {
        return $this->values;
    }

}
