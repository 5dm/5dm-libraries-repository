<?php
/**
 * Copyright (c) 2013 Michal Havlicek <michal.havlicek@email.cz>
 */

namespace FiveDM\Repository\v1\Client\Publisher\Model;
use Nette\Object;
use Nette\Utils\Strings;

/**
 * @author Michal Havlicek <michal.havlicek@email.cz>
 * @package Repository
 */
class Form extends Object
{
    /** @var int */
    protected $id;

    /** @var  array */
    protected $customer;

    /** @var  array */
    protected $values;

    /** @var  array */
    protected $source;

    /** @var  array */
    protected $utm;

    public static function from(array $form)
    {
        $instance = new static();

        if(isset($form['data'])) {
            $form['values'] = $form['data'];
            unset($form['data']);
        }

        // Store form in the repository

        $groupBy = function($prefix, array &$values) {

            $ret = array();

            foreach($values as $key => $value) {
                $key = Strings::lower($key);

                if(strpos($key, $prefix) === 0) {
                    $ret[str_replace($prefix,'',$key)] = $value;
                    unset($values[$key]);
                }
            }

            return $ret;
        };

        if(isset($form['meta'])) {
            $form['utm'] = $groupBy('utm', $form['meta']);
            $form['source'] = $form['meta'];
            unset($form['meta']);
        }

        foreach($form as $property => $value) {
            call_user_func(array($instance, 'set' . ucfirst($property)), $value);
        }

        return $instance;
    }

    public function toArray()
    {
        $meta = $this->source;

        foreach($this->utm as $utm => $value) {
            $meta['utm' . ucfirst($utm)] = $value;
        }

        return array(
            'customer' => $this->customer,
            'data'     => $this->values,
            'meta'     => $meta
        );
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param array $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return array
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param array $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return array
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param array $utm
     */
    public function setUtm($utm)
    {
        $this->utm = $utm;
    }

    /**
     * @return array
     */
    public function getUtm()
    {
        return $this->utm;
    }

    /**
     * @param array $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

}
