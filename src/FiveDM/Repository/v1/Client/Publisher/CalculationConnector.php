<?php
/**
 * Copyright (c) 2013 Michal Havlicek <michal.havlicek@email.cz>
 */

namespace FiveDM\Repository\v1\Client\Publisher;

/**
 * @author Michal Havlicek <michal.havlicek@email.cz>
 * @package Repository
 */
class CalculationConnector extends Connector
{
    /**
     * @param integer $campaignId ID of advertising campaign
     * @param array $credentials User and password
     */
    function __construct($campaignId, array $credentials)
    {
        parent::__construct($campaignId, $credentials);

        $this->options['url']['data']      = 'http://api.topsrovnani.cz/api/publisher/v1/<user>:<password>/data/<item>?cid=<cid>';
        $this->options['url']['pickOffer'] = 'http://api.topsrovnani.cz/api/publisher/v1/<user>:<password>/lead/pick-offer/<id>?cid=<cid>';
        $this->options['url']['offers']    = 'http://api.topsrovnani.cz/api/publisher/v1/<user>:<password>/lead/offers/<id>?cid=<cid>';
    }

    /**
     * @param $item
     * @param array $query
     * @return bool
     */
    public function getData($item, array $query = array())
    {
        try {
            $response = json_decode($this->createConnection($this->prepareUrl(array(
                'cid'  => $this->campaignId,
                'item' => $item
            ), 'data'))->get(http_build_query($query))->getResponse());

            return $response->meta->status == self::STATUS_OK  ? $response->data->$item : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }

    /**
     * @param $formId
     * @param $offer
     * @return bool
     */
    public function pickOffer($formId, $offer)
    {
        try {
            $response = json_decode(
                $this->createConnection($this->prepareUrl(array(
                    'cid' => $this->campaignId,
                    'id'  => $formId
                ), 'pickOffer'))->post(http_build_query(array('offer' => $offer)))->getResponse()
            );
            return $response->meta->status == self::STATUS_OK ? TRUE : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }

    /**
     * @param $formId
     * @return bool
     */
    public function getOffers($formId)
    {
        try {
            $response = json_decode(
                $this->createConnection($this->prepareUrl(array(
                    'cid' => $this->campaignId,
                    'id'  => $formId
                ), 'offers'))->get()->getResponse()
            );
            return $response->meta->status == self::STATUS_OK ? $response->data->offers : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }
}
