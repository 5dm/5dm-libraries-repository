<?php
/**
 * Copyright (c) 2013 Michal Havlicek <michal.havlicek@email.cz>
 */

namespace FiveDM\Repository\v1\Client\Publisher;

use FiveDM\Repository\v1\Client\Publisher\Model\Form;

/**
 * @author Michal Havlicek <michal.havlicek@email.cz>
 * @package Repository
 */
class Connector
{
    /** @var array */
    protected $options = array(
        'url' => array(
            'insert'  => 'http://api.topsrovnani.cz/api/publisher/v1/<user>:<password>/lead/insert?cid=<cid>',
            'update'  => 'http://api.topsrovnani.cz/api/publisher/v1/<user>:<password>/lead/update/<id>?cid=<cid>',
            'verify'  => 'http://api.topsrovnani.cz/api/publisher/v1/<user>:<password>/lead/verify/<id>?cid=<cid>',
            'smsCode' => 'http://api.topsrovnani.cz/api/publisher/v1/<user>:<password>/lead/resend-sms/<id>?cid=<cid>',
            'send'    => 'http://api.topsrovnani.cz/api/publisher/v1/<user>:<password>/lead/push/<id>?cid=<cid>',
            'queue'   => 'http://api.topsrovnani.cz/api/publisher/v1/<user>:<password>/lead/push-request/<id>?cid=<cid>'
    ));

    // Result statuses
    const STATUS_OK = 'success';
    const STATUS_ERROR = 'error';

    /** @var integer|null ID of advertising campaign */
    protected $campaignId = NULL;

    /** @var array Defines user and password */
    protected $credentials = array(
        'user'     => '',
        'password' => ''
    );

    /**
     * @param integer $campaignId ID of advertising campaign
     * @param array $credentials User and password
     */
    function __construct($campaignId, array $credentials)
    {
        $this->campaignId  = $campaignId;
        $this->credentials = $credentials;
    }

    /**
     * Connection configuration
     *
     * @param string $option Option to change
     * @param string $value New value
     */
    public function setOption($option, $value)
    {
        $this->options[$option] = $value;
    }

    /**
     * Method searches for placeholding patterns and replaces them with real values.
     *
     * @param array $parameters
     * @param string $url URL stub
     * @return string Returns URL with replaced patterns.
     */
    protected function prepareUrl(array $parameters, $url)
    {
        $url = $this->options['url'][$url];

        foreach(($parameters + $this->credentials) as $key => $value) {
            $url = str_replace('<' . $key . '>', $value, $url);
        }

        return $url;
    }

    /**
     * Method creates CURL request.
     *
     * @param string $url URL of API method
     * @return \Kdyby\Curl\Request Returns CURL wrapper
     */
    protected function createConnection($url)
    {
        $request = new \Kdyby\Curl\Request($url);
        $request->timeout = 120;

        return $request;
    }

    /**
     * Method creates new form in the repository.
     *
     * @param Model\Form $form Form that should be stored in repository
     * @return bool Returns true on success, otherwise false
     */
    public function create(Form $form)
    {
        try {
            $response = json_decode(
                $this->createConnection($this->prepareUrl(array(
                    'cid' => $this->campaignId
                ), 'insert'))->post(http_build_query($form->toArray()))->getResponse()
            );
            return $response->meta->status == self::STATUS_OK ? $response->data->id : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }

    /**
     * Method updates values of not-delivered form.
     *
     * @param integer $formId ID of form
     * @param Model\Form $form Form that should be updated
     * @return bool Returns true on success, otherwise false
     */
    public function update($formId, Form $form)
    {
        try {
            $response = json_decode(
                $this->createConnection($this->prepareUrl(array(
                    'cid' => $this->campaignId,
                    'id'  => $formId
                ), 'update'))->post(http_build_query($form->toArray()))->getResponse()
            );
            return $response->meta->status == self::STATUS_OK ? TRUE : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }

    /**
     * Method sends SMS verification code to customer's phone.
     *
     * @param integer $formId ID of form
     * @return bool Returns true on success, otherwise false
     */
    public function sendVerificationRequest($formId)
    {
        try {
            $response = json_decode($this->createConnection($this->prepareUrl(array(
                'cid' => $this->campaignId,
                'id'  => $formId
            ), 'smsCode'))->get()->getResponse());

            return $response->meta->status == self::STATUS_OK ? TRUE : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }

    /**
     * Method verifies SMS code inserted by user.
     *
     * @param integer $formId ID of form
     * @param integer $smsCode SMS code
     * @return bool Returns true on success, otherwise false
     */
    public function verify($formId, $smsCode)
    {
        try {
            $response = json_decode($this->createConnection($this->prepareUrl(array(
                'cid' => $this->campaignId,
                'id'  => $formId
            ), 'verify'))->post(http_build_query(array(
                'smsCode' => $smsCode
            )))->getResponse());

            return $response->meta->status == self::STATUS_OK ? TRUE : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }

    /**
     * Method delivers form to advertiser. This method triggers immediate push request, so no output is sent till
     * the advertiser's API confirms delivery.
     *
     * @param integer $formId ID of form
     * @return bool Returns true on success, otherwise false
     */
    public function send($formId)
    {
        try {
            $response = json_decode($this->createConnection($this->prepareUrl(array(
                'cid' => $this->campaignId,
                'id'  => $formId
            ), 'send'))->get()->getResponse());

            return $response->meta->status == self::STATUS_OK ? TRUE : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }

    /**
     * Method inserts deliver request at the end of the queue. That means, form will be delivered later.
     * Output message is sent back immediately.
     *
     * @param integer $formId ID of form
     * @return bool Returns true on success, otherwise false
     */
    public function queue($formId)
    {
        try {
            $response = json_decode($this->createConnection($this->prepareUrl(array(
                'cid' => $this->campaignId,
                'id'  => $formId
            ), 'queue'))->get()->getResponse());

            return $response->meta->status == self::STATUS_OK ? TRUE : FALSE;
        } catch(\Exception $e) {
            \Nette\Diagnostics\Debugger::log($e);
        }

        return FALSE;
    }
}
